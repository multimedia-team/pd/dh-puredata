#!/usr/bin/perl
# DH Sequence for pd-lib-builder.
use warnings;
use strict;
use Debian::Debhelper::Dh_Lib;

# Exclude pd documentation from dh_compress by default
add_command_options("dh_compress", qw(-X.pd -XLICENSE.txt));

# generate ${puredata:Depends} and friends
insert_before ("dh_installdeb", "dh_puredata_substvar");

1;
