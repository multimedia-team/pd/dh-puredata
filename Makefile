SCRIPTS = \
  dh_puredata_substvar	\
  $(empty)

MANPAGES = $(patsubst %,%.1,$(SCRIPTS))

VERSION := $(shell dpkg-parsechangelog -S Version)

.PHONY: all
all: manpages chmod

.PHONY: manpages
manpages: $(MANPAGES)

%.1: %
	pod2man --center="dh-puredata" --release="$(VERSION)" --utf8 $< > $@

.PHONY: chmod
chmod: $(SCRIPTS)
	chmod +x $(SCRIPTS)

.PHONY: clean
clean:
	rm -f $(MANPAGES)

