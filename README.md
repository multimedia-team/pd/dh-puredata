Debhelper-based infrastructure for building Debian packages for Pd externals
============================================================================

# Introduction

This package (dh-puredata) contains the infrastructure for building Debian
packages for externals packages for [Pure Data][1] which use the standard build
systems like [pd-lib-builder][2].

dh-puredata is completely based on [Debhelper][3].  It contains scripts
for automating of the building, installing, checking, and cleaning the
externals package.  It also generates the appropriate substitution
variables for use in `debian/control`.

[1]: https://puredata.info/
[2]: https://github.com/pure-data/pd-lib-builder/
[3]: https://github.com/Debian/debhelper

# Usage

Currently only `pd-lib-builder` based externals are supported.

## Usage for pd-lib-builder based externals

### Minimal usage

Add a Build-Depends stanza on the virtual package `dh-sequence-pd-lib-builder`
to your debian/control file:

```dep522
# [...]
Build-Depends:
 dh-sequence-pd-lib-builder,
# [...]
```

After that, a minimal debian/rules file like the following should
build the package:

```make
#!/usr/bin/make -f
# -*- makefile -*-

%:
	dh $@
```

If you need a specific feature, add an additional versioned build
dependency on 'dh-puredata'.

### More complex usage
For more complex usage, you can also add `--buildsystem=pd-lib-builder` (to
the dh invocation resp.  The invocation of specific sub-commands) and/or
`--with=pd-lib-builder` to the general 'dh' invocation.
In this case, you only need a Build-Depends on 'dh-puredata' (instead of
'dh-sequence-pd-lib-builder'):

# Internals of dh-puredata

The dh-puredata package adds a new buildsystem to the Debhelper system
through the files in `Debhelper/Buildsystem/`.
In these files, the `build`, `test`, `install` and `clean` targets of
Debhelper are redefined.

The dh-puredata package also alters the sequence of Debhelper commands
through the files in `Debhelper/Sequence/`.  It inserts calls to the `dh_puredata_*`
commands at the appropriate places, namely:

| dh-puredata command    | where  | Debhelper target       |
| ---------------------- | ------ | ---------------------- |
| `dh_puredata_substvar` | before | `dh_installdeb`        |

Notice that the commands above are not meant to be called directly by
the user, but are activated through the build-dependency on
dh-sequence-pd-lib-builder *OR* by using the option `--with=pd-lib-builder`
of `dh` in `debian/rules`.
